from flask import Flask ,request



app = Flask(__name__)
@app.route('/', methods=["GET"])
def index():
	return "This is test web server for iTechArt running on AWS EC2 instance with it's own vpc and with docker container from ECR "


if __name__ == "__main__":
	app.run(host='0.0.0.0',port=80, debug=True)
